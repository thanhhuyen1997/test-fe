var slideIndex = 1;
showSlides(slideIndex);
var timer;
// Next/previous controls
function plusSlides(n) {
  clearTimeout(timer);
  showSlides((slideIndex += n));
}

// Thumbnail image controls
function currentSlide(n) {
  clearTimeout(timer);
  showSlides((slideIndex = n));
}
// show slides
function showSlides(n) {
  var i;
  var dots = document.getElementsByClassName("dot");
  var slides = document.getElementsByClassName("mySlides");
  if (n == undefined) {
    n = ++slideIndex;
  }
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";
  timer = setTimeout(showSlides, 4000);
}

function stopShow() {
  clearTimeout(timer);
}
function runShow() {
  showSlides(slideIndex);
}

//Curve text around image
function circularText(radius, myClass, myDeg, myOrigin) {
  (txt = document.getElementsByClassName(myClass)[0].innerHTML),
    (classIndex = document.getElementsByClassName(myClass)[0]);
  txt = txt.split("");

  var deg = myDeg, // 6
    origin = myOrigin; //-75

  txt.forEach((ea) => {
    ea = `<p style='height:${radius}px;transform:rotate(${origin}deg);transform-origin:0 80%'>${ea}</p>`;
    classIndex.innerHTML += ea;
    origin += deg;
  });
}
circularText(90, "circTxt--1", 7, -165);
circularText(90, "circTxt--2", 15, 162);
circularText(90, "circTxt--3", 6, -175);
circularText(90, "circTxt--4", 15, 162);
circularText(90, "circTxt--5", 7, -172);
circularText(90, "circTxt--6", 15, 162);
circularText(90, "circTxt--7", 7, -172);
circularText(90, "circTxt--8", 15, 162);
